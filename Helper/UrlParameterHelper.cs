﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifySharp.Helper
{
    public static class UrlParameterHelper
    {
        public static Dictionary<string, string> ParseParameterList(string url)
        {
            var dict = new Dictionary<string, string>();

            // no parameters
            if (!url.Contains("?"))
                return dict;

            // get the parameters from the url
            var parameterList = url.Substring(url.IndexOf('?') + 1);

            // split single parameters
            var parmeters = parameterList.Split('&');

            // split name and value
            foreach (var chunk in parmeters)
            {
                var pair = chunk.Split('=');
                dict.Add(pair[0], pair[1]);
            }

            return dict;
        }

        public static String ParameterListToString(Dictionary<string, string> parameterList)
        {
            string result = parameterList.Aggregate("", (current, pair) => current + $"{pair.Key}={pair.Value}&");
            result = result.Substring(0, result.Length - 1);
            return result;
        }
    }
}
