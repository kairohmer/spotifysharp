﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SpotifySharp
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AlbumType
    {
        [EnumMember(Value = "album")]
        Album = 1,
        [EnumMember(Value = "single")]
        Single = 2,
        [EnumMember(Value = "compilation")]
        Compilation = 4,

        /// <summary>
        /// Actually not an album type, but allowed while requesting the albums of an artist.
        /// </summary>
        [EnumMember(Value = "appears_on")]
        AppearsOn = 8,
    }

    public class AlbumSimplified
    {
        /// <summary>
        /// The name of the album. In case of an album takedown, the value may be an empty string.
        /// </summary>
        [JsonProperty("name")]
        public readonly string Name;

        /// <summary>
        /// The type of the album: one of "Album", "Single", or "Compilation". 
        /// </summary>
        [JsonProperty("album_type")]
        public readonly AlbumType AlbumType;

        /// <summary>
        /// A link to the Web API endpoint providing full details of the track.
        /// </summary>
        [JsonProperty("href")]
        public readonly string Href;

        /// <summary>
        /// The Spotify URI for the album.
        /// </summary>
        [JsonProperty("uri")]
        public readonly string SpotifyUri;

        /// <summary>
        /// The Spotify ID for the album. 
        /// </summary>
        [JsonProperty("id")]
        public readonly string ID;

        /// <summary>
        /// The cover art for the album in various sizes, widest first.
        /// </summary>
        [JsonProperty("images")]
        public IReadOnlyList<Image> Images;

        public override string ToString()
        {
            return $"{Name}";
        }
    }

    public class Album : AlbumSimplified, IDetailedSpotifyType
    {
        /// <summary>
        /// The artists of the album. Each artist object includes a link in href to more detailed information about the artist.
        /// </summary>
        public IReadOnlyList<ArtistSimplified> Artists;

        /// <summary>
        /// A string that can be used for displaying the artist name.
        /// If there is only one artist the result is just this artists name.
        /// If there are multiple artists the result will be the name of the first artist followed by a 'et al.'.
        /// </summary>
        public String ArtistDisplayName => Artists.Count == 1 ? Artists[0].Name : $"{Artists[0].Name} et al.";

        /// <summary>
        /// The year the album was first released
        /// </summary>
        [JsonIgnore]
        public uint Year => uint.Parse(_ReleaseDate.Substring(0, 4));

        [JsonProperty("release_date")]
        private readonly string _ReleaseDate;

        /// <summary>
        /// The popularity of the album. The value will be between 0 and 100, with 100 being the most popular. 
        /// The popularity is calculated from the popularity of the album's individual tracks.
        /// </summary>
        [JsonProperty("popularity")]
        public readonly uint Popularity;

        /// <summary>
        /// A list of the genres used to classify the album.
        /// For example: "Prog Rock", "Post-Grunge". (If not yet classified, the array is empty.) 
        /// </summary>
        [JsonProperty("genres")]
        public IReadOnlyList<string> Genres;

        /// <summary>
        /// Number of tracks of this album.
        /// </summary>
        [JsonIgnore]
        public uint NumberOfTracks => (uint)Tracks.Total;

        /// <summary>
        /// The tracks of the album wrapped in an paging object.
        /// </summary>
        [JsonProperty("tracks")]
        public readonly PagingObject<TrackSimplified> Tracks;
    }
}
