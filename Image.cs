﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SpotifySharp
{
    public class Image
    {
        /// <summary>
        /// The image width in pixels. If unknown: 0
        /// </summary>
        [JsonProperty("width")]
        public readonly uint Width;

        /// <summary>
        /// The image height in pixels. If unknown: 0
        /// </summary>
        [JsonProperty("height")]
        public readonly uint Height;

        /// <summary>
        /// The source URL of the image. 
        /// </summary>
        [JsonProperty("url")]
        public readonly string Url;
        
    }
}
