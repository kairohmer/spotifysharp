﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SpotifySharp.Helper;

namespace SpotifySharp
{
    public class PagingObject<T>
    {
        /// <summary>
        /// A link to the Web API endpoint returning the full result of the request.
        /// </summary>
        [JsonProperty("href")]
        public readonly string Href;

        /// <summary>
        /// An array of objects	The requested data.
        /// </summary>
        [JsonProperty("items")]
        public IReadOnlyList<T> Items;

        /// <summary>
        /// Integer	The maximum number of items in the response (as set in the query or by default).
        /// </summary>
        [JsonProperty("limit")]
        public readonly int Limit;

        /// <summary>
        /// Offset	integer	The offset of the items returned (as set in the query or by default).
        /// </summary>
        [JsonProperty("offset")]
        public readonly int Offset;

        /// <summary>
        /// The total number of items available to return. 
        /// </summary>
        [JsonProperty("total")]
        public readonly int Total;

        /// <summary>
        /// URL to the next page of items. (null if none).
        /// </summary>
        [JsonProperty("next")]
        public readonly string Next;

        /// <summary>
        /// URL to the previous page of items. (null if none) 
        /// </summary>
        [JsonProperty("previous")]
        public readonly string Previous;

        /// <summary>
        /// Get the items from all pages, not just that one.
        /// The order is predefined so it's possible to define a offset and a number to request.
        /// </summary>
        /// <param name="offset">start at a specify item index (0 to request all items)</param>
        /// <param name="limit">numer of elements to request (-1 to request all items)</param>
        /// <returns>items form different pages</returns>
        public async Task<IReadOnlyList<T>> GetItemsFromAllPagesAsync(uint offset=0, int limit = -1)
        {
            Debug.Assert(limit != 0, "Limit can not be zero. Negative to get all items or positiv to get a certain number of items.");
            
            // replace offset and limit
            var parameters = UrlParameterHelper.ParseParameterList(Href);
            parameters["offset"] = offset.ToString();
            parameters["limit"] = (limit < 0 ? (int)Spotify.LimitMax : limit).ToString();
            string firstPageUrl = Href.Substring(0, Href.IndexOf('?') + 1) + UrlParameterHelper.ParameterListToString(parameters);

            // get the first page
            var page = (await WebApiQuery.GetAsync(firstPageUrl)).Deserialize<PagingObject<T>>();
            List<T> items = new List<T>();
            var targetCount = (limit > 0 ? limit : page.Total) - offset;

            while (true)
            {
                // add all items from the current page
                if(items.Count + page.Items.Count <= targetCount)
                    items.AddRange(page.Items);

                // add only the required items
                else
                    for (int i = 0; i < targetCount - items.Count; i++)
                        items.Add(page.Items[i]);

                // stop if this was the last page or the requested number of items is reached
                if (String.IsNullOrEmpty(page.Next) || items.Count == targetCount)
                    break;
                
                // load the next page
                page = (await WebApiQuery.GetAsync(page.Next)).Deserialize<PagingObject<T>>();
            }

            return items;
        }

        /// <summary>
        /// Get one item from any of the pages.
        /// </summary>
        /// <param name="index">the index of the item [0, Total-1]</param>
        /// <returns></returns>
        public async Task<T> GetItemAsync(uint index)
        {
            Debug.Assert(index < Total, "Index out of Bounds.");
            return (await GetItemsFromAllPagesAsync(index, 1))[0];
        }
    }
}
