﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SpotifySharp.Helper
{
    public class WebApiQueryResponse
    {
        public HttpStatusCode StatusCode
        {
            get { return WebResponse != null ? WebResponse.StatusCode : HttpStatusCode.InternalServerError; }
        }

        public HttpWebResponse WebResponse = null;
        public string JsonString;
  
        public JObject ToJsonObject()
        {
            return JObject.Parse(JsonString);
        }

        public T Deserialize<T>()
        {
            return JsonConvert.DeserializeObject<T>(JsonString);
        }
    }

    public static class WebApiQuery
    {
        public static async Task<WebApiQueryResponse> GetAsync(string queryUrl)
        {
            WebResponse response = null;
            var result = new WebApiQueryResponse();

            try
            {
                // start requesting
                var request = WebRequest.Create(queryUrl);
                response = await request.GetResponseAsync();    // get a valid response
            }
            catch (WebException wex)
            {
                response = wex.Response;                        // get a error response
            }

            // handle web responses
            result.WebResponse = response as HttpWebResponse;
            if (result.WebResponse != null)
            {
                // failed?
                if (result.WebResponse.StatusCode != HttpStatusCode.OK)
                    return result;

                // read the result
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    result.JsonString = await reader.ReadToEndAsync();
            }
            return result;
        }
    }
}
