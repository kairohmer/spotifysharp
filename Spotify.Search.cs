﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpotifySharp.Extensions;
using SpotifySharp.Helper;

namespace SpotifySharp
{
    public partial class Spotify
    {
        public async Task<PagingObject<AlbumSimplified>> SearchAlbumByArtistAsync(string artist, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            artist = EncodeForUrl(artist);
            return await QueryAlbumAsync($"artist:{artist}&type=album", offset, limit);
        }

        public async Task<PagingObject<AlbumSimplified>> SearchAlbumByArtistAsync(string artist, string album, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            artist = EncodeForUrl(artist);
            album = EncodeForUrl(album);
            return await QueryAlbumAsync($"artist:{artist}%20album:{album}&type=album", offset, limit);
        }

        public async Task<PagingObject<AlbumSimplified>> SearchAlbumAsync(string album, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            album = EncodeForUrl(album);
            return await QueryAlbumAsync($"album:{album}&type=album", offset, limit);
        }

        public async Task<PagingObject<AlbumSimplified>> QueryAlbumAsync(string query, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            Debug.Assert(limit >= Spotify.LimitMin && limit <= Spotify.LimitMax, $"Value of 'limit' has to be in [{Spotify.LimitMin},{Spotify.LimitMax}]");
            Debug.Assert(offset <= Spotify.OffsetMax, $"Value of 'offset' has to be in [{Spotify.OffsetMin},{Spotify.OffsetMax}]");

            var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}search?q={query}&offset={offset}&limit={limit}");
            return response.ToJsonObject()["albums"].ToObject<PagingObject<AlbumSimplified>>();
        }



        public async Task<PagingObject<ArtistSimplified>> SearchArtistAsync(string artist, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            artist = EncodeForUrl(artist);
            return await QueryArtistAsync($"artist:{artist}&type=artist", offset, limit);
        }

        public async Task<PagingObject<ArtistSimplified>> QueryArtistAsync(string query, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            Debug.Assert(limit >= Spotify.LimitMin && limit <= Spotify.LimitMax, $"Value of 'limit' has to be in [{Spotify.LimitMin},{Spotify.LimitMax}]");
            Debug.Assert(offset <= Spotify.OffsetMax, $"Value of 'offset' has to be in [{Spotify.OffsetMin},{Spotify.OffsetMax}]");

            var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}search?q={query}&offset={offset}&limit={limit}");
            return response.ToJsonObject()["artists"].ToObject<PagingObject<ArtistSimplified>>();
        }

        public async Task<PagingObject<PlaylistSimplfied>> SearchPlaylistAsync(string playlistName, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            playlistName = EncodeForUrl(playlistName);
            return await QueryPlaylistAsync($"{playlistName}&type=playlist", offset, limit);
        }

        public async Task<PagingObject<PlaylistSimplfied>> QueryPlaylistAsync(string query, uint offset = Spotify.OffsetDefault, uint limit = Spotify.LimitDefault)
        {
            Debug.Assert(limit >= Spotify.LimitMin && limit <= Spotify.LimitMax, $"Value of 'limit' has to be in [{Spotify.LimitMin},{Spotify.LimitMax}]");
            Debug.Assert(offset <= Spotify.OffsetMax, $"Value of 'offset' has to be in [{Spotify.OffsetMin},{Spotify.OffsetMax}]");

            var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}search?q={query}&offset={offset}&limit={limit}");
            return response.ToJsonObject()["playlists"].ToObject<PagingObject<PlaylistSimplfied>>();
        }

        private string EncodeForUrl(string s)
        {
            s = s.Replace("\"", "%22");
            s = s.Replace(' ', '+');
            return s;
        }
    }
}
