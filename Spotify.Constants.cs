﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifySharp
{
    public partial class Spotify
    {
        internal const uint LimitMin = 1;
        internal const uint LimitDefault = 20;
        internal const uint LimitMax = 50;

        internal const uint OffsetMin = 0;
        internal const uint OffsetDefault = 0;
        internal const uint OffsetMax = 100000;

        internal const uint GetMultipleTracksMax = 50;
        internal const uint GetMultipleAlbumsMax = 20;
        internal const uint GetMultipleArtistsMax = 50;

        internal const string ApiUrl = "https://api.spotify.com/v1/";

        internal static string GetControllerName<T>() where T : IDetailedSpotifyType
        {
            string typeName = typeof (T).Name.ToLower();
            return typeName + "s";
        }
    }
}
