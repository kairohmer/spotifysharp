﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SpotifySharp.Extensions;
using SpotifySharp.Helper;

namespace SpotifySharp
{
    public class ArtistSimplified
    {
        /// <summary>
        /// The name of the artist.
        /// </summary>
        [JsonProperty("name")]
        public readonly string Name;

        /// <summary>
        /// A link to the Web API endpoint providing full details of the artist.
        /// </summary>
        [JsonProperty("href")]
        public readonly string Href;

        /// <summary>
        /// The Spotify URI for the artist.
        /// </summary>
        [JsonProperty("uri")]
        public readonly string SpotifyUri;

        /// <summary>
        /// The Spotify ID for the artist. 
        /// </summary>
        [JsonProperty("id")]
        public readonly string ID;

        /// <summary>
        /// Get Spotify catalog information about an artist’s albums.
        /// </summary>
        /// <param name="types">optionally, you can filter the request by AlbumType</param>
        /// <param name="offset">start a certain index</param>
        /// <param name="limit">number of albums to get</param>
        /// <returns>an array of simplified album objects (wrapped in a paging object)</returns>
        public async Task<PagingObject<AlbumSimplified>> GetAlbumsAsync(
            AlbumType types = AlbumType.Album | AlbumType.Single | AlbumType.Compilation | AlbumType.AppearsOn,
            uint offset = Spotify.OffsetDefault, 
            uint limit = Spotify.LimitDefault)
        {
            var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}artists/{ID}/albums?album_type={StringExt.CommaSeparetedList(types.GetFlags()).ToLower()}&offset={offset}&limit={limit}");
            return response.Deserialize<PagingObject<AlbumSimplified>>();
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }

    public class Artist : ArtistSimplified, IDetailedSpotifyType
    {
        /// <summary>
        /// The artists of the album. 
        /// Each artist object includes a link in href to more detailed information about the artist.
        /// </summary>
        [JsonProperty("artists")]
        public IReadOnlyList<ArtistSimplified> Artists;

        /// <summary>
        /// The popularity of the artist. The value will be between 0 and 100, with 100 being the most popular.
        /// The artist's popularity is calculated from the popularity of all the artist's tracks.
        /// </summary>
        [JsonProperty("popularity")]
        public readonly uint Popularity;

        /// <summary>
        /// A list of the genres the artist is associated with. 
        /// For example: "Prog Rock", "Post-Grunge". (If not yet classified, the array is empty.) 
        /// </summary>
        [JsonProperty("genres")]
        public IReadOnlyList<string> Genres;

        /// <summary>
        /// Images of the artist in various sizes, widest first.
        /// </summary>
        [JsonProperty("images")]
        public IReadOnlyList<Image> Images;
    }
}
