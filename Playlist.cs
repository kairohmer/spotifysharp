﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SpotifySharp
{
    public class PlaylistSimplfied
    {
        /// <summary>
        /// The name of the playlist.
        /// </summary>
        [JsonProperty("name")]
        public readonly string Name;

        /// <summary>
        /// A link to the Web API endpoint providing full details of the playlist.
        /// </summary>
        [JsonProperty("href")]
        public readonly string Href;

        /// <summary>
        /// The Spotify URI for the playlist.
        /// </summary>
        [JsonProperty("uri")]
        public readonly string SpotifyUri;

        /// <summary>
        /// The Spotify ID for the playlist. 
        /// </summary>
        [JsonProperty("id")]
        public readonly string ID;

        /// <summary>
        /// Images for the playlist. The array may be empty or contain up to three images. The images are returned by size in descending order. See Working with Playlists.
        /// Note: If returned, the source URL for the image(url) is temporary and will expire in less than a day.
        /// </summary>
        [JsonProperty("images")]
        public IReadOnlyList<Image> Images;

        /// <summary>
        /// true if the owner allows other users to modify the playlist.
        /// </summary>
        [JsonProperty("collaborative")]
        public readonly bool Collaborative;
        
        /// <summary>
        /// The playlist's public/private status: true the playlist is public, false the playlist is private, null the playlist status is not relevant. For more about public/private status, see Working with Playlists.
        /// </summary>
        [JsonProperty("public")]
        public readonly bool? Public;

        [JsonProperty("tracks")]
        private dynamic _Tracks; // also contains a link, but it looks like authorization is required

        /// <summary>
        /// The total number of tracks in the playlist.
        /// </summary>
        [JsonIgnore]
        public uint TotalTracks { get { return (uint)_Tracks["total"]; } }

        public override string ToString()
        {
            return $"{Name}";
        }
    }

    public class Playlist : PlaylistSimplfied, IDetailedSpotifyType
    {
        internal Playlist()
        {
            
        }
    }
}
