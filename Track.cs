﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SpotifySharp.Extensions;
using SpotifySharp.Helper;

namespace SpotifySharp
{
    public class TrackSimplified
    {
        /// <summary>
        /// The name of the track. 
        /// </summary>
        [JsonProperty("name")]
        public readonly string Name;

        /// <summary>
        /// The artists who performed the track. 
        /// Each artist object includes a link in href to more detailed information about the artist. 
        /// </summary>
        [JsonProperty("artists")]
        public IReadOnlyList<ArtistSimplified> Artists;

        /// <summary>
        /// A string that can be used for displaying the artist name.
        /// If there is only one artist the result is just this artists name.
        /// If there are multiple artists the result will be the name of the first artist followed by a 'et al.'.
        /// </summary>
        public String ArtistDisplayName => Artists.Count == 1 ? Artists[0].Name : $"{Artists[0].Name} et al.";

        /// <summary>
        /// The number of the track. If an album has several discs, the track number is the number on the specified disc 
        /// </summary>
        [JsonProperty("track_number")]
        public readonly uint TrackNumber;

        /// <summary>
        /// The disc number (usually 1 unless the album consists of more than one disc). 
        /// </summary>
        [JsonProperty("disc_number")]
        public readonly uint DiscNumber;

        /// <summary>
        /// The track length.
        /// </summary>
        public TimeSpan Duration => TimeSpan.FromMilliseconds(_DurationMs);

        [JsonProperty("duration_ms")]
        private readonly uint _DurationMs;

        /// <summary>
        /// Whether or not the track has explicit lyrics (true = yes it does; false = no it does not OR unknown). 
        /// </summary>
        [JsonProperty("explicit")]
        public readonly bool Explicit;

        /// <summary>
        /// A link to the Web API endpoint providing full details of the track.
        /// </summary>
        [JsonProperty("href")]
        public readonly string Href;

        /// <summary>
        /// The Spotify ID for the track. 
        /// </summary>
        [JsonProperty("id")]
        public readonly string ID;

        /// <summary>
        /// The Spotify URI for the track.
        /// </summary>
        [JsonProperty("uri")]
        public readonly string SpotifyUri;

        /// <summary>
        /// A link to a 30 second preview (MP3 format) of the track.
        /// </summary>
        [JsonProperty("preview_url")]
        public readonly string PreviewUrl;

        public override string ToString()
        {
            return $"{Name}";
        }
    }


    public class Track : TrackSimplified, IDetailedSpotifyType
    {
        /// <summary>
        /// The album on which the track appears. 
        /// The album object includes a link in href to full information about the album. 
        /// </summary>
        [JsonProperty("album")]
        public AlbumSimplified Album;

        /// <summary>
        /// The popularity of the track. The value will be between 0 and 100, with 100 being the most popular.
        /// The popularity of a track is a value between 0 and 100, with 100 being the most popular.The popularity is calculated by algorithm and is based, in the most part, on the total number of plays the track has had and how recent those plays are.
        /// Generally speaking, songs that are being played a lot now will have a higher popularity than songs that were played a lot in the past. Duplicate tracks (e.g.the same track from a single and an album) are rated independently.Artist and album popularity is derived mathematically from track popularity.Note that the popularity value may lag actual popularity by a few days: the value is not updated in real time..
        /// </summary>
        [JsonProperty("popularity")]
        public uint Popularity;
    }
}
