﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifySharp.Extensions
{
    public static class StringExt
    {
        public static string ToCamelCaseSegment(this string s)
        {
            s = s.ToLower();
            return s.Substring(0, 1).ToUpper() + s.Substring(1);
        }

        public static String CommaSeparetedList<T>(IEnumerable<T> enumerable)
        {
            string result = enumerable.Aggregate("", (current, flag) => current + (flag.ToString() + ","));
            return result.Substring(0, result.Length - 1); // remove the last ','
        }
    }
}
