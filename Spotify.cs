﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpotifySharp.Extensions;
using SpotifySharp.Helper;

namespace SpotifySharp
{
    public interface IDetailedSpotifyType { }

    public partial class Spotify
    {
        /// <summary>
        /// Gets a full (detailed, NOT simplified) album, track, artist, ... by ID.
        /// </summary>
        /// <param name="spotifyID">identifies the album to get</param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(string spotifyID) where T : IDetailedSpotifyType
        {

            var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}{GetControllerName<T>()}/{spotifyID}");
            return response.Deserialize<T>();
        }

        /// <summary>
        /// Gets a full album.
        /// </summary>
        /// <param name="simplifiedAlbum">the simplfied version of the album to get</param>
        /// <returns></returns>
        public async Task<Album> GetAsync(AlbumSimplified simplifiedAlbum)
        {
            return await GetAsync<Album>(simplifiedAlbum.ID);
        }

        /// <summary>
        /// Gets multiple full albums.
        /// </summary>
        /// <param name="simplifiedAlbums">the simplfied versions of the albums to get</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Album>> GetAsync(IEnumerable<AlbumSimplified> simplifiedAlbums)
        {
            var list = new List<Album>();

            for (int i = 0; i < simplifiedAlbums.Count(); i += (int)Spotify.GetMultipleAlbumsMax)
            {
                var ids = StringExt.CommaSeparetedList(simplifiedAlbums.Select(t => t.ID).Skip(i).Take((int)Spotify.GetMultipleAlbumsMax));
                var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}albums/?ids={ids}");
                var albums = response.ToJsonObject()["albums"].ToObject<IReadOnlyList<Album>>();
                list.AddRange(albums);
            }
            return list;
        }

        /// <summary>
        /// Gets full artist information.
        /// </summary>
        /// <param name="simplifiedArtist">the simplfied version of the artist to get</param>
        /// <returns></returns>
        public async Task<Artist> GetAsync(ArtistSimplified simplifiedArtist)
        {
            return await GetAsync<Artist>(simplifiedArtist.ID);
        }


        /// <summary>
        /// Gets multiple full artist information.
        /// </summary>
        /// <param name="simplifiedArtists">the simplfied versions of the artists to get</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Artist>> GetAsync(IEnumerable<ArtistSimplified> simplifiedArtists)
        {
            var list = new List<Artist>();

            for (int i = 0; i < simplifiedArtists.Count(); i += (int)Spotify.GetMultipleArtistsMax)
            {
                var ids = StringExt.CommaSeparetedList(simplifiedArtists.Select(t => t.ID).Skip(i).Take((int)Spotify.GetMultipleArtistsMax));
                var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}artists/?ids={ids}");
                var artists = response.ToJsonObject()["artists"].ToObject<IReadOnlyList<Artist>>();
                list.AddRange(artists);
            }
            return list;
        }

        /// <summary>
        /// Gets full track information.
        /// </summary>
        /// <param name="simplifiedTrack">the simplfied version of the track to get</param>
        /// <returns></returns>
        public async Task<Track> GetAsync(TrackSimplified simplifiedTrack)
        {
            return await GetAsync<Track>(simplifiedTrack.ID);
        }

        /// <summary>
        /// Gets multiple full track information.
        /// </summary>
        /// <param name="simplifiedTracks">the simplfied versions of the tracks to get</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Track>> GetAsync(IEnumerable<TrackSimplified> simplifiedTracks)
        {
            var list = new List<Track>();

            for (int i = 0; i < simplifiedTracks.Count(); i += (int)Spotify.GetMultipleTracksMax)
            {
                var ids = StringExt.CommaSeparetedList(simplifiedTracks.Select(t => t.ID).Skip(i).Take((int)Spotify.GetMultipleTracksMax));
                var response = await WebApiQuery.GetAsync($"{Spotify.ApiUrl}tracks/?ids={ids}");
                var tracks = response.ToJsonObject()["tracks"].ToObject<IReadOnlyList<Track>>();
                list.AddRange(tracks);
            }
            return list;
        }

        /// <summary>
        /// Gets full playlist information.
        /// </summary>
        /// <param name="simplifiedPlaylist">the simplfied version of the playlist to get</param>
        /// <returns></returns>
        public async Task<Playlist> GetAsync(PlaylistSimplfied simplifiedPlaylist)
        {
            return await GetAsync<Playlist>(simplifiedPlaylist.ID);
        }
    }
}
